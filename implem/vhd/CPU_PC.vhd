library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.PKG.all;


entity CPU_PC is
    generic(
        mutant: integer := 0
    );
    Port (
        -- Clock/Reset
        clk    : in  std_logic ;
        rst    : in  std_logic ;

        -- Interface PC to PO
        cmd    : out PO_cmd ;
        status : in  PO_status
    );
end entity;

architecture RTL of CPU_PC is
    type State_type is (
        S_Error,
        S_Init,
        S_Pre_Fetch,
        S_Fetch,
        S_Decode,
	S_Mem,
	S_WSW,
	S_LUI,
	S_ADDI,
	S_ADD,
	S_AND,
	S_ANDI,
	S_OR,
	S_ORI,
	S_XOR,
	S_XORI,
	S_SUB,
	S_SLL,
	S_SRL,
	S_SRA,
	S_SRAI,
	S_SLLI,
	S_SRLI,
	S_AUIPC,
	S_BEQ,
	S_BNE,
	S_BLT,
	S_BGE,
	S_BLTU,
	S_BGEU,
	S_SLT,
	S_SLTI,
	S_SLTU,
	S_SLTIU,
	S_LW,
	S_SW,
	S_SWAdr,
	S_JAL,
	S_JALR
    );

    signal state_d, state_q : State_type;
    signal cmd_cs : PO_cs_cmd;


    function arith_sel (IR : unsigned( 31 downto 0 ))
        return ALU_op_type is
        variable res : ALU_op_type;
    begin
        if IR(30) = '0' or IR(5) = '0' then
            res := ALU_plus;
        else
            res := ALU_minus;
        end if;
        return res;
    end arith_sel;

    function logical_sel (IR : unsigned( 31 downto 0 ))
        return LOGICAL_op_type is
        variable res : LOGICAL_op_type;
    begin
        if IR(12) = '1' then
            res := LOGICAL_and;
        else
            if IR(13) = '1' then
                res := LOGICAL_or;
            else
                res := LOGICAL_xor;
            end if;
        end if;
        return res;
    end logical_sel;

    function shifter_sel (IR : unsigned( 31 downto 0 ))
        return SHIFTER_op_type is
        variable res : SHIFTER_op_type;
    begin
        res := SHIFT_ll;
        if IR(14) = '1' then
            if IR(30) = '1' then
                res := SHIFT_ra;
            else
                res := SHIFT_rl;
            end if;
        end if;
        return res;
    end shifter_sel;

begin

    cmd.cs <= cmd_cs;

    FSM_synchrone : process(clk)
    begin
        if clk'event and clk='1' then
            if rst='1' then
                state_q <= S_Init;
            else
                state_q <= state_d;
            end if;
        end if;
    end process FSM_synchrone;

    FSM_comb : process (state_q, status)
    begin

        -- Valeurs par défaut de cmd à définir selon les préférences de chacun
        cmd.rst               <= 'U';
        cmd.ALU_op            <= UNDEFINED;
        cmd.LOGICAL_op        <= UNDEFINED;
        cmd.ALU_Y_sel         <= UNDEFINED;

        cmd.SHIFTER_op        <= UNDEFINED;
        cmd.SHIFTER_Y_sel     <= UNDEFINED;

        cmd.RF_we             <= '0';
        cmd.RF_SIZE_sel       <= UNDEFINED;
        cmd.RF_SIGN_enable    <= 'U';
        cmd.DATA_sel          <= DATA_from_alu;

        cmd.PC_we             <= '0';
        cmd.PC_sel            <= PC_from_alu;

        cmd.PC_X_sel          <= PC_X_pc;
        cmd.PC_Y_sel          <= PC_Y_immU;

        cmd.TO_PC_Y_sel       <= TO_PC_Y_cst_x04; --cst4

        cmd.AD_we             <= 'U';
        cmd.AD_Y_sel          <= UNDEFINED;

        cmd.IR_we             <= 'U';

        cmd.ADDR_sel          <= ADDR_from_pc;
        cmd.mem_we            <= '0';
        cmd.mem_ce            <= '0';

        cmd_cs.CSR_we            <= UNDEFINED;

        cmd_cs.TO_CSR_sel        <= UNDEFINED;
        cmd_cs.CSR_sel           <= UNDEFINED;
        cmd_cs.MEPC_sel          <= UNDEFINED;

        cmd_cs.MSTATUS_mie_set   <= 'U';
        cmd_cs.MSTATUS_mie_reset <= 'U';

        cmd_cs.CSR_WRITE_mode    <= UNDEFINED;

        state_d <= state_q;

        case state_q is
            when S_Error =>
                state_d <= S_Error;

            when S_Init =>
                -- PC <- RESET_VECTOR
                cmd.PC_we <= '1';
                cmd.PC_sel <= PC_rstvec;
                state_d <= S_Pre_Fetch;

            when S_Pre_Fetch =>
                -- mem[PC]
                cmd.mem_ce <= '1';
                state_d <= S_Fetch;

            when S_Fetch =>
                -- IR <- mem_datain
                cmd.IR_we <= '1';
                state_d <= S_Decode;

	    when S_Mem =>
		cmd.mem_ce <= '1';
		cmd.ADDR_sel <= ADDR_from_ad;
		cmd.RF_we <='1';
		cmd.RF_SIZE_sel <= RF_SIZE_word;
		cmd.DATA_sel <= DATA_from_mem;
		state_d <= S_Pre_Fetch;

	    when S_WSW =>
		cmd.ADDR_sel <= ADDR_from_ad;
		cmd.mem_ce <= '1';
		cmd.mem_we <= '1';
		state_d <= S_Pre_Fetch;

            when S_Decode =>

		state_d <= S_Error; 
                -- PC <- PC + 4
		if status.IR(6 downto 0) = "1100011" then
		    if status.IR(14 downto 12) = "000" then
			cmd.TO_PC_Y_sel <= TO_PC_Y_cst_x04;
		    	cmd.PC_sel <= PC_from_pc;
		    	cmd.ALU_Y_SEL<= ALU_Y_rf_rs2;
		    	cmd.PC_we <= '0';
		    	state_d <= S_BEQ;
		    end if;
		    if status.IR(14 downto 12) = "001" then
			cmd.TO_PC_Y_sel <= TO_PC_Y_cst_x04;
		    	cmd.PC_sel <= PC_from_pc;
		    	cmd.ALU_Y_SEL<= ALU_Y_rf_rs2;
		    	cmd.PC_we <= '0';
			state_d <= S_BNE;
		    end if;
		    if status.IR(14 downto 12) = "100" then
			cmd.TO_PC_Y_sel <= TO_PC_Y_cst_x04;
		    	cmd.PC_sel <= PC_from_pc;
		    	cmd.ALU_Y_SEL<= ALU_Y_rf_rs2;
		    	cmd.PC_we <= '0';
			state_d <= S_BLT;
		    end if;
		    if status.IR(14 downto 12) = "101" then
			cmd.TO_PC_Y_sel <= TO_PC_Y_cst_x04;
		    	cmd.PC_sel <= PC_from_pc;
		    	cmd.ALU_Y_SEL<= ALU_Y_rf_rs2;
		    	cmd.PC_we <= '0';
			state_d <= S_BGE;
		    end if;
		    if status.IR(14 downto 12) = "110" then
			cmd.TO_PC_Y_sel <= TO_PC_Y_cst_x04;
		    	cmd.PC_sel <= PC_from_pc;
		    	cmd.ALU_Y_SEL<= ALU_Y_rf_rs2;
		    	cmd.PC_we <= '0';
			state_d <= S_BLTU;
		    end if;
		    if status.IR(14 downto 12) = "111" then
			cmd.TO_PC_Y_sel <= TO_PC_Y_cst_x04;
		    	cmd.PC_sel <= PC_from_pc;
		    	cmd.ALU_Y_SEL<= ALU_Y_rf_rs2;
		    	cmd.PC_we <= '0';
			state_d <= S_BGEU;
		    end if;
	    	end if;
                if status.IR(6 downto 0) = "0110111" then
		    cmd.TO_PC_Y_sel <= TO_PC_Y_cst_x04;
		    cmd.PC_sel <= PC_from_pc;
		    cmd.PC_we <= '1';
		    state_d <= S_LUI;
	    	end if;
		if status.IR(6 downto 0) = "0010111" then
		    cmd.TO_PC_Y_sel <= TO_PC_Y_cst_x04;
		    cmd.PC_sel <= PC_from_pc;
		    cmd.PC_we <= '1';
		    state_d <= S_AUIPC;
	    	end if;
		if status.IR(6 downto 0) = "0010011" then
		    if status.IR(14 downto 12) = "000" then
			cmd.TO_PC_Y_sel <= TO_PC_Y_cst_x04;
			cmd.PC_sel <= PC_from_pc;
			cmd.PC_we <= '1';
			state_d <= S_ADDI;
		    end if;
		    if status.IR(14 downto 12) = "001" then
			cmd.TO_PC_Y_sel <= TO_PC_Y_cst_x04;
			cmd.PC_sel <= PC_from_pc;
			cmd.PC_we <= '1';
			state_d <= S_SLLI;
		    end if;
		    if status.IR(14 downto 12) = "010" then
			cmd.TO_PC_Y_sel <= TO_PC_Y_cst_x04;
		        cmd.PC_sel <= PC_from_pc;
		        cmd.PC_we <= '1';
		        state_d <= S_SLTI;
		    end if;
		    if status.IR(14 downto 12) = "011" then
			cmd.TO_PC_Y_sel <= TO_PC_Y_cst_x04;
		        cmd.PC_sel <= PC_from_pc;
		        cmd.PC_we <= '1';
		        state_d <= S_SLTIU;
		    end if;
		    if status.IR(14 downto 12) = "110" then
			cmd.TO_PC_Y_sel <= TO_PC_Y_cst_x04;
			cmd.PC_sel <= PC_from_pc;
			cmd.PC_we <= '1';
			state_d <= S_ORI;
		    end if;
		    if status.IR(14 downto 12) = "100" then
			cmd.TO_PC_Y_sel <= TO_PC_Y_cst_x04;
			cmd.PC_sel <= PC_from_pc;
			cmd.PC_we <= '1';
			state_d <= S_XORI;
		    end if;
		    if status.IR(14 downto 12) = "101" then
			if status.IR(31 downto 25) = "0000000" then
				cmd.TO_PC_Y_sel <= TO_PC_Y_cst_x04;
				cmd.PC_sel <= PC_from_pc;
				cmd.PC_we <= '1';
				state_d <= S_SRLI;
			end if;
			if status.IR(31 downto 25) = "0100000" then
				cmd.TO_PC_Y_sel <= TO_PC_Y_cst_x04;
				cmd.PC_sel <= PC_from_pc;
				cmd.PC_we <= '1';
				state_d <= S_SRAI;
			end if;
		    end if;
		    if status.IR(14 downto 12) = "111" then
			cmd.TO_PC_Y_sel <= TO_PC_Y_cst_x04;
			cmd.PC_sel <= PC_from_pc;
			cmd.PC_we <= '1';
			state_d <= S_ANDI;
		    end if;
	    	end if;
		if status.IR(6 downto 0) = "0110011" then
		    if status.IR(14 downto 12) = "010" then
			cmd.TO_PC_Y_sel <= TO_PC_Y_cst_x04;
		        cmd.PC_sel <= PC_from_pc;
		        cmd.PC_we <= '1';
		        state_d <= S_SLT;
		    end if;
		    if status.IR(14 downto 12) = "011" then
			cmd.TO_PC_Y_sel <= TO_PC_Y_cst_x04;
		        cmd.PC_sel <= PC_from_pc;
		        cmd.PC_we <= '1';
		        state_d <= S_SLTU;
		    end if;
		    if status.IR(14 downto 12) = "101" then
			if status.IR(31 downto 25) = "0000000" then
				cmd.TO_PC_Y_sel <= TO_PC_Y_cst_x04;
				cmd.PC_sel <= PC_from_pc;
				cmd.PC_we <= '1';
				state_d <= S_SRL;
			end if;
			if status.IR(31 downto 25) = "0100000" then
				cmd.TO_PC_Y_sel <= TO_PC_Y_cst_x04;
				cmd.PC_sel <= PC_from_pc;
				cmd.PC_we <= '1';
				state_d <= S_SRA;
			end if;
		    end if;
		    if status.IR(14 downto 12) = "000" then
			if status.IR(31 downto 25) = "0000000" then
			    cmd.TO_PC_Y_sel <= TO_PC_Y_cst_x04;
			    cmd.PC_sel <= PC_from_pc;
			    cmd.PC_we <= '1';
			    state_d <= S_ADD;
			end if;
			if status.IR(31 downto 25) = "0100000" then
			    cmd.TO_PC_Y_sel <= TO_PC_Y_cst_x04;
			    cmd.PC_sel <= PC_from_pc;
			    cmd.PC_we <= '1';
			    state_d <= S_SUB;
			end if;
		    end if;
		    if status.IR(14 downto 12) = "111" then
			if status.IR(31 downto 25) = "0000000" then
			    cmd.TO_PC_Y_sel <= TO_PC_Y_cst_x04;
			    cmd.PC_sel <= PC_from_pc;
			    cmd.PC_we <= '1';
			    state_d <= S_AND;
			end if;
		    end if;
		    if status.IR(14 downto 12) = "110" then
			if status.IR(31 downto 25) = "0000000" then
			    cmd.TO_PC_Y_sel <= TO_PC_Y_cst_x04;
			    cmd.PC_sel <= PC_from_pc;
			    cmd.PC_we <= '1';
			    state_d <= S_OR;
			end if;
		    end if;
		    if status.IR(14 downto 12) = "100" then
			if status.IR(31 downto 25) = "0000000" then
			    cmd.TO_PC_Y_sel <= TO_PC_Y_cst_x04;
			    cmd.PC_sel <= PC_from_pc;
			    cmd.PC_we <= '1';
			    state_d <= S_XOR;
			end if;
		    end if;
		    if status.IR(14 downto 12) = "001" then
			if status.IR(31 downto 25) = "0000000" then
			    cmd.TO_PC_Y_sel <= TO_PC_Y_cst_x04;
			    cmd.PC_sel <= PC_from_pc;
			    cmd.PC_we <= '1';
			    state_d <= S_SLL;
			end if;
		    end if;
	    	end if;
		if status.IR(6 downto 0) = "0000011" then
			cmd.TO_PC_Y_sel <= TO_PC_Y_cst_x04;
			cmd.PC_sel <= PC_from_pc;
			cmd.AD_Y_sel <= AD_Y_immI;
			cmd.AD_we <='1';
			cmd.PC_we <= '1';
			state_d <= S_LW;
	    	end if;
		if status.IR(6 downto 0) = "0100011" then
			cmd.TO_PC_Y_sel <= TO_PC_Y_cst_x04;
			cmd.PC_sel <= PC_from_pc;
			cmd.PC_we <= '1';
			state_d <= S_SWAdr;
	    	end if;
		if status.IR(6 downto 0) = "1101111" then
		    	state_d <= S_JAL;
	    	end if;
		if status.IR(6 downto 0) = "1100111" then
		    	state_d <= S_JALR;
	    	end if;
                -- Décodage effectif des instructions,
                -- à compléter par vos soins

---------- Instructions avec immediat de type U ----------

	   when S_LUI =>
		-- rd <- ImmU + 0
		cmd.PC_X_sel <= PC_X_cst_x00;
		cmd.PC_Y_sel <= PC_Y_immU;
		cmd.RF_we <= '1';
		cmd.DATA_sel <= DATA_from_pc;
		-- lecture mem[PC]
		cmd.ADDR_sel <= ADDR_from_pc;
		cmd.mem_ce <= '1';
		cmd.mem_we <= '0';
		-- next state
		state_d <= S_Fetch;

	   when S_AUIPC =>
		-- rd <- ImmU + PC
		cmd.PC_X_sel <= PC_X_pc;
		cmd.PC_Y_sel <= PC_Y_immU;
		cmd.RF_we <= '1';
		cmd.DATA_sel <= DATA_from_pc;
		-- lecture mem[PC]
		cmd.ADDR_sel <= ADDR_from_pc;
		cmd.mem_ce <= '1';
		cmd.mem_we <= '0';
		-- next state
		state_d <= S_Fetch;

---------- Instructions arithmétiques et logiques ----------

	  when S_ADDI =>
		--Select Imm
		cmd.ALU_Y_sel <= ALU_Y_immI;
		--Select de addition
		cmd.ALU_op <= ALU_plus;
		--rd <- rs1+Imm
		cmd.RF_we <= '1';
		cmd.DATA_sel <= DATA_from_alu;
		-- lecture de la mem[PC]
		cmd.ADDR_sel <= ADDR_from_pc;
		cmd.mem_ce <= '1';
		cmd.mem_we <= '0';
		--next state
		state_d <= S_Fetch;

	  when S_ANDI =>
		--Select Imm
		cmd.ALU_Y_sel <= ALU_Y_immI;
		--Select de addition
		cmd.LOGICAL_op <= LOGICAL_and;
		--rd <- rs1+Imm
		cmd.RF_we <= '1';
		cmd.DATA_sel <= DATA_from_logical;
		-- lecture de la mem[PC]
		cmd.ADDR_sel <= ADDR_from_pc;
		cmd.mem_ce <= '1';
		cmd.mem_we <= '0';
		--next state
		state_d <= S_Fetch;

	  when S_ORI =>
		--Select Imm
		cmd.ALU_Y_sel <= ALU_Y_immI;
		--Select de addition
		cmd.LOGICAL_op <= LOGICAL_or;
		--rd <- rs1+Imm
		cmd.RF_we <= '1';
		cmd.DATA_sel <= DATA_from_logical;
		-- lecture de la mem[PC]
		cmd.ADDR_sel <= ADDR_from_pc;
		cmd.mem_ce <= '1';
		cmd.mem_we <= '0';
		--next state
		state_d <= S_Fetch;

	  when S_XORI =>
		--Select Imm
		cmd.ALU_Y_sel <= ALU_Y_immI;
		--Select de addition
		cmd.LOGICAL_op <= LOGICAL_xor;
		--rd <- rs1+Imm
		cmd.RF_we <= '1';
		cmd.DATA_sel <= DATA_from_logical;
		-- lecture de la mem[PC]
		cmd.ADDR_sel <= ADDR_from_pc;
		cmd.mem_ce <= '1';
		cmd.mem_we <= '0';
		--next state
		state_d <= S_Fetch;

	  when S_ADD =>
		--Select rs2
		cmd.ALU_Y_sel <= ALU_Y_rf_rs2;
		--Select de addition
		cmd.ALU_op <= ALU_plus;
		--rd <- rs1+rs2
		cmd.RF_we <= '1';
		cmd.DATA_sel <= DATA_from_alu;
		-- lecture de la mem[PC]
		cmd.ADDR_sel <= ADDR_from_pc;
		cmd.mem_ce <= '1';
		cmd.mem_we <= '0';
		--next state
		state_d <= S_Fetch;

	  when S_SUB =>
		--Select rs2
		cmd.ALU_Y_sel <= ALU_Y_rf_rs2;
		--Select de addition
		cmd.ALU_op <= ALU_minus;
		--rd <- rs1+rs2
		cmd.RF_we <= '1';
		cmd.DATA_sel <= DATA_from_alu;
		-- lecture de la mem[PC]
		cmd.ADDR_sel <= ADDR_from_pc;
		cmd.mem_ce <= '1';
		cmd.mem_we <= '0';
		--next state
		state_d <= S_Fetch;

	  when S_AND =>
	  	--Select rs2
		cmd.ALU_Y_sel <= ALU_Y_rf_rs2;
		--Select de addition
		cmd.LOGICAL_op <= LOGICAL_and;
		--rd <- rs1 & rs2
		cmd.RF_we <= '1';
		cmd.DATA_sel <= DATA_from_logical;
		-- lecture de la mem[PC]
		cmd.ADDR_sel <= ADDR_from_pc;
		cmd.mem_ce <= '1';
		cmd.mem_we <= '0';
		--next state
		state_d <= S_Fetch;

	  when S_OR =>
		--Select rs2
	  	cmd.ALU_Y_sel <= ALU_Y_rf_rs2;
		--Select de addition
		cmd.LOGICAL_op <= LOGICAL_or;
		--rd <- rs1 || rs2
		cmd.RF_we <= '1';
		cmd.DATA_sel <= DATA_from_logical;
		-- lecture de la mem[PC]
		cmd.ADDR_sel <= ADDR_from_pc;
		cmd.mem_ce <= '1';
		cmd.mem_we <= '0';
		--next state
		state_d <= S_Fetch;

	  when S_XOR =>
		--Select rs2
	  	cmd.ALU_Y_sel <= ALU_Y_rf_rs2;
		--Select de addition
		cmd.LOGICAL_op <= LOGICAL_xor;
		--rd <- rs1 || rs2
		cmd.RF_we <= '1';
		cmd.DATA_sel <= DATA_from_logical;
		-- lecture de la mem[PC]
		cmd.ADDR_sel <= ADDR_from_pc;
		cmd.mem_ce <= '1';
		cmd.mem_we <= '0';
		--next state
		state_d <= S_Fetch;

	  when S_SLL =>
		--Select rs2
		cmd.SHIFTER_Y_sel<= SHIFTER_Y_rs2;
		--Select de addition
		cmd.SHIFTER_op <= SHIFT_ll;
		--rd <- rs1+rs2
		cmd.RF_we <= '1';
		cmd.DATA_sel <= DATA_from_shifter;
		-- lecture de la mem[PC]
		cmd.ADDR_sel <= ADDR_from_pc;
		cmd.mem_ce <= '1';
		cmd.mem_we <= '0';
		--next state
		state_d <= S_Fetch;

	  when S_SRL =>
		--Select rs2
		cmd.SHIFTER_Y_sel<= SHIFTER_Y_rs2;
		--Select de addition
		cmd.SHIFTER_op <= SHIFT_rl;
		--rd <- rs1+rs2
		cmd.RF_we <= '1';
		cmd.DATA_sel <= DATA_from_shifter;
		-- lecture de la mem[PC]
		cmd.ADDR_sel <= ADDR_from_pc;
		cmd.mem_ce <= '1';
		cmd.mem_we <= '0';
		--next state
		state_d <= S_Fetch;

	  when S_SRA =>
		--Select rs2
		cmd.SHIFTER_Y_sel<= SHIFTER_Y_rs2;
		--Select de addition
		cmd.SHIFTER_op <= SHIFT_ra;
		--rd <- rs1+rs2
		cmd.RF_we <= '1';
		cmd.DATA_sel <= DATA_from_shifter;
		-- lecture de la mem[PC]
		cmd.ADDR_sel <= ADDR_from_pc;
		cmd.mem_ce <= '1';
		cmd.mem_we <= '0';
		--next state
		state_d <= S_Fetch;

	  when S_SRAI =>
		--Select rs2
		cmd.SHIFTER_Y_sel<= SHIFTER_Y_ir_sh;
		--Select de addition
		cmd.SHIFTER_op <= SHIFT_ra;
		--rd <- rs1+rs2
		cmd.RF_we <= '1';
		cmd.DATA_sel <= DATA_from_shifter;
		-- lecture de la mem[PC]
		cmd.ADDR_sel <= ADDR_from_pc;
		cmd.mem_ce <= '1';
		cmd.mem_we <= '0';
		--next state
		state_d <= S_Fetch;

	  when S_SRLI =>
		--Select rs2
		cmd.SHIFTER_Y_sel<= SHIFTER_Y_ir_sh;
		--Select addition
		cmd.SHIFTER_op <= SHIFT_rl;
		--rd <- rs1+rs2
		cmd.RF_we <= '1';
		cmd.DATA_sel <= DATA_from_shifter;
		-- lecture mem[PC]
		cmd.ADDR_sel <= ADDR_from_pc;
		cmd.mem_ce <= '1';
		cmd.mem_we <= '0';
		--next state
		state_d <= S_Fetch;

	  when S_SLLI =>
		--Select rs2
		cmd.SHIFTER_Y_sel<= SHIFTER_Y_ir_sh;
		--Select addition
		cmd.SHIFTER_op <= SHIFT_ll;
		--rd <- rs1+rs2
		cmd.RF_we <= '1';
		cmd.DATA_sel <= DATA_from_shifter;
		-- lecture mem[PC]
		cmd.ADDR_sel <= ADDR_from_pc;
		cmd.mem_ce <= '1';
		cmd.mem_we <= '0';
		--next state
		state_d <= S_Fetch;

	  when S_SLT =>
		cmd.ALU_Y_SEL<= ALU_Y_rf_rs2;
		--rd <- rs1+rs2
		cmd.RF_we <= '1';
		cmd.DATA_sel <= DATA_from_slt;
		-- lecture mem[PC]
		cmd.ADDR_sel <= ADDR_from_pc;
		cmd.mem_ce <= '1';
		cmd.mem_we <= '0';
		--next state
		state_d <= S_Fetch;

	  when S_SLTU =>
		cmd.ALU_Y_SEL<= ALU_Y_rf_rs2;
		--rd <- rs1+rs2
		cmd.RF_we <= '1';
		cmd.DATA_sel <= DATA_from_slt;
		-- lecture mem[PC]
		cmd.ADDR_sel <= ADDR_from_pc;
		cmd.mem_ce <= '1';
		cmd.mem_we <= '0';
		--next state
		state_d <= S_Fetch;

	  when S_SLTI =>
		cmd.ALU_Y_SEL<= ALU_Y_immI;
		--rd <- rs1+rs2
		cmd.RF_we <= '1';
		cmd.DATA_sel <= DATA_from_slt;
		-- lecture mem[PC]
		cmd.ADDR_sel <= ADDR_from_pc;
		cmd.mem_ce <= '1';
		cmd.mem_we <= '0';
		--next state
		state_d <= S_Fetch;

	  when S_SLTIU =>
		cmd.ALU_Y_SEL<= ALU_Y_immI;
		--rd <- rs1+rs2
		cmd.RF_we <= '1';
		cmd.DATA_sel <= DATA_from_slt;
		-- lecture mem[PC]
		cmd.ADDR_sel <= ADDR_from_pc;
		cmd.mem_ce <= '1';
		cmd.mem_we <= '0';
		--next state
		state_d <= S_Fetch;

---------- Instructions de saut ----------
	  when S_BEQ =>
		
		cmd.PC_sel<= PC_from_pc;
		cmd.ALU_Y_SEL<= ALU_Y_rf_rs2;
		if status.JCOND=true then
			cmd.TO_PC_Y_sel<= TO_PC_Y_immB;
		else
			cmd.TO_PC_Y_sel<= TO_PC_Y_cst_x04;
		end if;
		
		cmd.PC_we <= '1';

		-- lecture mem[PC]
		cmd.ADDR_sel <= ADDR_from_pc;
		cmd.mem_ce <= '1';
		cmd.mem_we <= '0';
		--next state
		state_d <= S_Pre_Fetch;

	  when S_BNE =>
		
		cmd.PC_sel<= PC_from_pc;
		cmd.ALU_Y_SEL<= ALU_Y_rf_rs2;
		if status.JCOND=true then
			cmd.TO_PC_Y_sel<= TO_PC_Y_immB;
		else
			cmd.TO_PC_Y_sel<= TO_PC_Y_cst_x04;
		end if;
		
		cmd.PC_we <= '1';

		-- lecture mem[PC]
		cmd.ADDR_sel <= ADDR_from_pc;
		cmd.mem_ce <= '1';
		cmd.mem_we <= '0';
		--next state
		state_d <= S_Pre_Fetch;

	  when S_BGE =>
		
		cmd.PC_sel<= PC_from_pc;
		cmd.ALU_Y_SEL<= ALU_Y_rf_rs2;
		if status.JCOND=true then
			cmd.TO_PC_Y_sel<= TO_PC_Y_immB;
		else
			cmd.TO_PC_Y_sel<= TO_PC_Y_cst_x04;
		end if;
		
		cmd.PC_we <= '1';

		-- lecture mem[PC]
		cmd.ADDR_sel <= ADDR_from_pc;
		cmd.mem_ce <= '1';
		cmd.mem_we <= '0';
		--next state
		state_d <= S_Pre_Fetch;

	  when S_BLT =>
		
		cmd.PC_sel<= PC_from_pc;
		cmd.ALU_Y_SEL<= ALU_Y_rf_rs2;
		if status.JCOND=true then
			cmd.TO_PC_Y_sel<= TO_PC_Y_immB;
		else
			cmd.TO_PC_Y_sel<= TO_PC_Y_cst_x04;
		end if;
		
		cmd.PC_we <= '1';

		-- lecture mem[PC]
		cmd.ADDR_sel <= ADDR_from_pc;
		cmd.mem_ce <= '1';
		cmd.mem_we <= '0';
		--next state
		state_d <= S_Pre_Fetch;

	  when S_BLTU =>
		
		cmd.PC_sel<= PC_from_pc;
		cmd.ALU_Y_SEL<= ALU_Y_rf_rs2;
		if status.JCOND=true then
			cmd.TO_PC_Y_sel<= TO_PC_Y_immB;
		else
			cmd.TO_PC_Y_sel<= TO_PC_Y_cst_x04;
		end if;
		
		cmd.PC_we <= '1';

		-- lecture mem[PC]
		cmd.ADDR_sel <= ADDR_from_pc;
		cmd.mem_ce <= '1';
		cmd.mem_we <= '0';
		--next state
		state_d <= S_Pre_Fetch;

	  when S_BGEU =>
		
		cmd.PC_sel<= PC_from_pc;
		cmd.ALU_Y_SEL<= ALU_Y_rf_rs2;
		if status.JCOND=true then
			cmd.TO_PC_Y_sel<= TO_PC_Y_immB;
		else
			cmd.TO_PC_Y_sel<= TO_PC_Y_cst_x04;
		end if;
		
		cmd.PC_we <= '1';

		-- lecture mem[PC]
		cmd.ADDR_sel <= ADDR_from_pc;
		cmd.mem_ce <= '1';
		cmd.mem_we <= '0';
		--next state
		state_d <= S_Pre_Fetch;

	  when S_JAL =>		

		cmd.DATA_sel <= DATA_from_pc;
		cmd.PC_X_sel <= PC_X_pc;
		cmd.PC_Y_sel <= PC_Y_cst_x04;
		cmd.RF_we <= '1';
		
		cmd.TO_PC_Y_sel <= TO_PC_Y_immJ;
		cmd.PC_sel <= PC_from_pc;
		cmd.PC_we <= '1';
		--next state
		state_d <= S_Pre_Fetch;

	  when S_JALR =>		

		cmd.DATA_sel <= DATA_from_pc;
		cmd.PC_X_sel <= PC_X_pc;
		cmd.PC_Y_sel <= PC_Y_cst_x04;
		cmd.RF_we <= '1';
		
		cmd.ALU_Y_sel <= ALU_Y_immI;
		cmd.ALU_op <= ALU_plus;
		cmd.PC_sel <= PC_from_alu;
		cmd.PC_we <= '1';
		--next state
		state_d <= S_Pre_Fetch;

---------- Instructions de chargement à part de la mémoire ----------

	  when S_LW =>
		cmd.ADDR_sel <= ADDR_from_ad;
		cmd.mem_ce <= '1';
		cmd.mem_we <= '0';
		cmd.DATA_sel <= DATA_from_mem;
		state_d <= S_Mem;

---------- Instructions de sauvegarde en mémoire ----------

	  when S_SW =>
		cmd.ADDR_sel <= ADDR_from_ad;
		cmd.mem_ce <= '1';
		cmd.mem_we <= '1';
		state_d <= S_WSW;
	  when S_SWAdr =>
		cmd.AD_Y_sel <= AD_Y_immS;
		cmd.AD_we <='1';
		state_d <= S_SW;
		
---------- Instructions d'accès aux CSR ----------

            when others => null;
        end case;

    end process FSM_comb;

end architecture;
