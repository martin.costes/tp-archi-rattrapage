library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.PKG.all;

entity CPU_CND is
    generic (
        mutant      : integer := 0
    );
    port (
        ina         : in w32; --rs1
        inb         : in w32; --rs2
        f           : in  std_logic; --IR_q(14)
        r           : in  std_logic; --IR_q(13)
        e           : in  std_logic; --IR_q(12)
        d           : in  std_logic; --IR_q(6)
        s           : out std_logic; --SLT
        j           : out std_logic  --JMP
    );
end entity;

architecture RTL of CPU_CND is
	
begin
    
    process(all)
        begin
	    s<='0';
	    j<='0';
	    if f='0' then
		if r ='0' then
			if e='0' then
				if d='1' then
					if ina=inb then
						j<='1'; --beq
					end if;	
		   	        end if;
			else
				if d='1' then
					if ina/=inb then
						j<='1'; --bne
					end if;	
		   	        end if;
		        end if;
		else
			if e='0' then
				if d='0' then
					if ina<inb then
						s<='1'; --slt et slti
					end if;	
		   	        end if;
			else
				if d='0' then
					if unsigned(ina)<unsigned(inb) then
						s<='1'; --sltu et sltiu
					end if;	
		   	        end if;
		        end if;
				        
		    end if;
	    else
		if r ='1' then
			if e='0' then
				if d='1' then
					if unsigned(ina)<unsigned(inb) then    --bltu
						j<='1';
					end if;			
		   	        end if;
			else
				if d='1' then
					if unsigned(ina)>=unsigned(inb) then  --bgeu
						j<='1';
					end if;				
		   	        end if;
		        end if;
		else
			if e='0' then
				if d='1' then
					if signed(ina)<signed(inb) then --blt
						j<='1';
					end if;	
		   	        end if;
			else
				if d='1' then
					if signed(ina)>=signed(inb) then  --bge
						j<='1';
					end if;	
		   	        end if;
		        end if;
				        
		end if;
	    end if;
    end process;
end architecture;
