# TAG = addi
	.text

	addi x31, x0, 0  	#Test d'une addition avec une constante nulle
	addi x31, x0, 7  	#Test d'une addition avec une constante positive
	addi x31, x31, 10  	#Test d'une addition avec une constante positive et un registre non nul
	addi x29, x31, -1  	#Test d'une addition avec une constante négative
	addi x31, x29, 0
	
	# max_cycle 50
	# pout_start
	# 00000000
	# 00000007
	# 00000011
	# 00000010
	# pout_end
