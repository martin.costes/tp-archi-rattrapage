# TAG = slt
	.text

	addi x30, x0, 9  	#Test d'une addition avec une constante nulle
	addi x29, x0, 1  	#Test d'une addition avec une constante nulle
	slt x31, x29, x30
	slt x31, x30, x29
	slt x31, x30, x30
	
	# max_cycle 50
	# pout_start
	# 00000001
	# 00000000
	# 00000000
	# pout_end
