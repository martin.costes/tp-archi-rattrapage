# TAG = sub
	.text

	addi x30, x0, 1  	#Test d'une addition avec une constante nulle
	addi x29, x0, 4  	#Test d'une addition avec une constante positive
	addi x28, x0, -2  	#Test d'une addition avec une constante positive
	addi x27, x0, 2
	sub x31, x30, x0
	sub x31, x29, x30
	sub x31, x29, x28
	sub x31, x30, x27
	
	
	# max_cycle 50
	# pout_start
	# 00000001
	# 00000003
	# 00000006
	# ffffffff
	# pout_end
