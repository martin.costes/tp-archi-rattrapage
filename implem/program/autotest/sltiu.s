# TAG = sltiu
	.text

	addi x30, x0, 9  	#Test d'une addition avec une constante nulle
	addi x29, x0, 1  	#Test d'une addition avec une constante nulle
	addi x28, x0, -9  	#Test d'une addition avec une constante nulle
	sltiu x31, x29, -9
	sltiu x31, x28, 1
	sltiu x31, x28, 9
	
	# max_cycle 50
	# pout_start
	# 00000001
	# 00000000
	# 00000000
	# pout_end
