# TAG = add
	.text

	addi x30, x0, 1  	#Test d'une addition avec une constante nulle
	addi x29, x0, 4  	#Test d'une addition avec une constante positive
	addi x28, x0, -2  	#Test d'une addition avec une constante négative
	add x31, x30, x0
	add x31, x29, x30
	add x31, x28, x30
	add x31, x28, x29
	
	
	# max_cycle 50
	# pout_start
	# 00000001
	# 00000005
	# ffffffff
	# 00000002
	# pout_end
